# Wenglor weCat3D API for LabVIEW

VI suite for plain ethernet communication via EthernetScanner DLL/Shared Library with WENGLOR MLSL sensors

The VIs in this repository are created and provided by HAMPEL SOFTWARE ENGINEERING (HSE, www.hampel-soft.com).

## :rocket: Installation

> Don't go looking for a VI Package - there is none! If you're wondering why, read more about [how we work with reuse code at HSE](https://dokuwiki.hampel-soft.com/code/common/dependency-structure).

Just copy everything under the folder `Source` into your project structure and use it.

### :wrench: LabVIEW 2014

The VIs are maintained in LabVIEW 2014.

### :link: Dependencies

These libraries depend on the WENGLOR EthernetScanner DLL for weCat3D MLSL/MLWL.


## :bulb: Usage

You can find more information about our file and project structure at https://dokuwiki.hampel-soft.com/code/common/project-structure.


## :busts_in_silhouette: Contributing 

We welcome every and any contribution. On our Dokuwiki, we compiled detailed information on 
[how to contribute](https://dokuwiki.hampel-soft.com/processes/collaboration). 
Please get in touch at (office@hampel-soft.com) for any questions.


##  :beers: Credits

* Joerg Hampel


## :page_facing_up: License 

This project is licensed under a modified BSD License - see the [LICENSE](LICENSE) file for details.
